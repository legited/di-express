import { buildContainer } from '../../src/web-server/countainer-builder';
import { Tenants } from '../../src/config/tenants';
import { expect } from 'chai';
import { AccountsServiceTag } from '../../src/accounts/account.service';
import { AcmeAccountService } from '../../src/accounts/acme-account.service';
import { LibertyAccountService } from '../../src/accounts/liberty-account.service';

describe('buildContainer', () => {
  it('builds acme container', () => {
    const container = buildContainer(Tenants.acme);
    const acmeAccountSevice = container.get<AcmeAccountService>(AccountsServiceTag);
    // tslint:disable-next-line:chai-vague-errors
    expect(acmeAccountSevice instanceof AcmeAccountService).to.eq(true);
  });

  it('builds liberty container', () => {
    const container = buildContainer(Tenants.liberty);
    const acmeAccountSevice = container.get<LibertyAccountService>(AccountsServiceTag);
    // tslint:disable-next-line:chai-vague-errors
    expect(acmeAccountSevice instanceof LibertyAccountService).to.eq(true);
  });
});
