import 'reflect-metadata';
import { IWebServer, ExpressServer } from './web-server';

async function main() {
  const websServer: IWebServer = new ExpressServer();
  await websServer.start();

  process.on('beforeExit', async () => {
    await websServer.stop();
  });
}

main()
  .then(() => {
    console.info('App started');
  })
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
