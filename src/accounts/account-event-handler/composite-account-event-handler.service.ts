import {
  IAccountEventHandler,
  IAccountEvent,
  AccountEventHandlersTag
} from '.';
import { injectable, multiInject } from 'inversify';

@injectable()
export class CompositeAccountEventHandler implements IAccountEventHandler {
  private readonly handlers: IAccountEventHandler[];
  constructor(
    @multiInject(AccountEventHandlersTag) eventHandlers: IAccountEventHandler[]
  ) {
    this.handlers = eventHandlers;
  }

  public handle(event: IAccountEvent) {
    this.handlers.forEach(handler => handler.handle(event));
  }
}
