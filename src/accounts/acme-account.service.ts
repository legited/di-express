import { IAccountService } from './account.service';
import { IAccount, Account } from './account.model';
import { injectable } from 'inversify';

// tslint:disable-next-line:completed-docs
@injectable()
export class AcmeAccountService implements IAccountService {
  public getAll(): IAccount[] {
    return [new Account(1, 2000, 'acme')];
  }
}
