export interface IAccount {
  id: number;
  balance: number;
  bank: string;
}

/**
 * Account model
 */
export class Account implements IAccount {
  public id: number;
  public balance: number;
  public bank: string;
  constructor(id: number, balance: number, bank: string) {
    this.id = id;
    this.balance = balance;
    this.bank = bank;
  }
}
