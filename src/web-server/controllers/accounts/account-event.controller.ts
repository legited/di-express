import { injectable, inject } from 'inversify';
import {
  AccountEventHandlerTag,
  IAccountEvent,
  IAccountEventHandler
} from '../../../accounts/account-event-handler';

@injectable()
export class AccountEventsController {
  private readonly eventHandler: IAccountEventHandler;
  constructor(
    @inject(AccountEventHandlerTag) eventHandler: IAccountEventHandler
  ) {
    this.eventHandler = eventHandler;
  }
  public handleEvent(event: IAccountEvent) {
    this.eventHandler.handle(event);
  }
}

export const AccountEventsControllerTag = Symbol(AccountEventsController.name);
