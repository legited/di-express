import { Request, Response, NextFunction } from 'express';
import { forEach } from 'lodash';
import { Tenants } from '../../../config/tenants';
import { buildContainer } from '../../countainer-builder';
import { Container } from 'inversify';

/**
 * DI container middleware
 * @middlware
 */
export function diContainer() {
  const containers: { [tenant: string]: Container } = {};
  forEach(Tenants, tenant => {
    const container: Container = buildContainer(tenant);
    containers[tenant] = container;
  });

  return (req: Request, _res: Response, next: NextFunction) => {
    const tenant = req.context.tenant;
    const container = containers[tenant];

    if (!container) {
      next(new Error('Tenant container not found'));
    }

    req.context.container = container;

    next();
  };
}
