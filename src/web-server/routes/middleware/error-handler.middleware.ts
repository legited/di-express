import { Response, NextFunction } from 'express';
import { IHttpError, httpStatus } from '../../../http/error';

export function errorHandler(
  error: IHttpError,
  _req: Request,
  res: Response,
  _next: NextFunction
): void {
  console.info('handled error');
  res
    .status(error.status || httpStatus.INTERNAL_SERVER_ERROR)
    .json(error.message);
}
