import { Request, Response, NextFunction } from 'express';
import { set } from 'lodash';
import { HttpError, httpStatus } from '../../../http/error';

export function tenantResolver(
  req: Request,
  _res: Response,
  next: NextFunction
) {
  const tenant = req.headers.tenant;

  if (!tenant) {
    return next(new HttpError('Tenant not found', httpStatus.UNPROCESSABLE_ENTITY));
  }
  set(req, 'context.tenant', tenant);

  next();
}
