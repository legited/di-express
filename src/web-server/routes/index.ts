import { Router } from 'express';
import bodyParser from 'body-parser';

import { accountsRouter } from './accounts.router';
import { errorHandler } from './middleware/error-handler.middleware';
import { tenantResolver } from './middleware/tenant-resolver.middleware';
import { diContainer } from './middleware/di.middleware';

export const appRouter = Router();
appRouter.use(bodyParser.json());
appRouter.use(tenantResolver);
appRouter.use(diContainer());
appRouter.use('/accounts', accountsRouter);
appRouter.use(<any>errorHandler);
