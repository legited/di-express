import e from 'express';
import { Server } from 'http';
import { appRouter } from './routes';

export interface IWebServer {
  start(): Promise<void>;
  stop(): Promise<void>;
}

/**
 * App web server
 */
export class ExpressServer implements IWebServer {
  private server: Server;

  public async start(): Promise<void> {
    console.time('Web server started in');
    const app = e();
    app.use(appRouter);

    return new Promise(resolve => {
      this.server = app.listen(1337);
      this.server.on('listening', () => {
        console.info('Server listening on port 1337');
        console.timeEnd('Web server started in');
        resolve();
      });
    });
  }

  public async stop(): Promise<void> {
    return new Promise(resolve => {
      this.server.close(() => {
        resolve();
      });
    });
  }
}
